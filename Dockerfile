FROM python:3.6-slim

LABEL maintainer="mark.hutchison89@gmail.com"

COPY . /AWSEC2

WORKDIR /AWSEC2

RUN pip install --no-cache-dir -r requirements.txt

RUN ["pytest", "-v", "--color=yes", "code/test.py"]

CMD tail -f /dev/null